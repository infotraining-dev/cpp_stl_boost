#include <iostream>
#include <queue>
#include <string>
#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

class Worker
{
    typedef boost::function<void ()> CommandType;
    queue<CommandType> tasks_;

    boost::function<void (int)> on_finish_tasks_;
public:
    void register_task(CommandType cmd)
    {
        tasks_.push(cmd);
    }

    void register_callback(boost::function<void (int)> callback)
    {
        on_finish_tasks_ = callback;
    }

    void run()
    {
        size_t count = 0;

        while(!tasks_.empty())
        {
            CommandType cmd = tasks_.front();
            cmd();
            ++count;
            tasks_.pop();
        }

        if (on_finish_tasks_)
            on_finish_tasks_(count); // wywolanie callbacka
    }
};

class Printer
{
public:
    void print(const string& text)
    {
        cout << "Print:" << text << endl;
    }

    void off()
    {
        cout << "Printer.off\n";
    }

    void on()
    {
        cout << "Printer.on\n";
    }
};

void save_to_db(int count)
{
    cout << "Save to db: " << count << endl;
}

class Logger
{
public:
    void log(const string& msg)
    {
        cout << "Logging: " << msg << endl;
    }
};

int main()
{
    Logger logger;

    Printer prn;

    Worker worker;

    worker.register_task(boost::bind(&Printer::on, &prn));
    worker.register_task(boost::bind(&Printer::print, &prn, "Tekst"));
    worker.register_task(boost::bind(&Printer::off, &prn));

    //...

    boost::function<void(int)> logging_lambda =
            [&logger](int count) { logger.log("Ilosc zadan: "
                                    + boost::lexical_cast<string>(count)); };

    //worker.register_callback(&save_to_db);
    worker.register_callback(logging_lambda);

    worker.run();

    return 0;
}

