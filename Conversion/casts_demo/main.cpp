#include <iostream>
#include <boost/cast.hpp>
#include <boost/any.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_io.hpp>

using namespace std;

boost::tuple<int, int> divide(int x, int y)
{
    return boost::make_tuple(x / y, x % y);
}

boost::tuple<int, string> create_values()
{
    return boost::make_tuple(5, "Text");
}

int main()
{
    short sx;

    int x = 667;

    sx = boost::numeric_cast<short>(x);

    cout << sx << endl;

    unsigned int ux = boost::numeric_cast<unsigned int>(x);

    cout << ux << endl;

    cout << "\n------------------\n";

    boost::any a;

    a = 1;
    a = 3.14;
    a = string("Text");
    a = make_pair(1, "One");

    pair<int, const char*> p
            = boost::any_cast<pair<int, const char*> >(a);

    cout << p.first << " - " << p.second << endl;

    a = 34;

    cout << "a type: " << a.type().name() << endl;

    int* number = boost::any_cast<int>(&a);

    if (number)
        cout << "number: " << *number << endl;

    cout << "\n-------------------\n";

    boost::tuple<int, int> result = divide(11, 3);

    cout << result << endl;

    int r = result.get<0>();
    int d = boost::get<1>(result);

    x = 19;
    string str = "tekst";

    boost::tuple<int&, string&> ref_t(x, str);

    ref_t.get<0>() = 55;

    cout << "x = " << x << endl;

    ref_t = create_values();

    cout << "x = " << x  << "; str = " << str << endl;

    int wynik;
    //int reszta;

    boost::tie(wynik, boost::tuples::ignore) = divide(18, 4);

    cout << "wynik: " << wynik << "  reszta: " << reszta << endl;
}

