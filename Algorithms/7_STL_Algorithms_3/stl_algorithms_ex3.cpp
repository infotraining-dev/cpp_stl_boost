#include "utils.hpp"

#include <vector>
#include <random>
#include <set>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>
#include <boost/bind.hpp>

using namespace std;

class RandGen
{
    int range_;
    random_device& rd;
    mt19937& gen;
    uniform_int_distribution<> dis;
public:
    RandGen(int range, random_device& rd, mt19937& gen) : range_(range), rd(rd), gen(gen), dis(0, range)
    {
    }

    int operator()() const
    {
        return dis(gen);
    }
};

int main()
{
    random_device rd;
    mt19937 gen;

    vector<int> vec(25);
    generate(vec.begin(), vec.end(), RandGen(30, rd, gen));

    vector<int> sorted_vec(vec);
    sort(sorted_vec.begin(), sorted_vec.end());

    multiset<int> mset(vec.begin(), vec.end());

    print(vec, "vec: ");
    print(sorted_vec, "sorted_vec: ");
    print(mset, "mset: ");

    // czy 17 znajduje sie w sekwencji?
    cout << "jest 17: " << (find(vec.begin(), vec.end(), 17) != vec.end()) << endl;
    cout << "jest 17: " << binary_search(sorted_vec.begin(), sorted_vec.end(), 17) << endl;
    cout << "jest 17: " << (mset.find(17) != mset.end()) << endl;

    // ile razy wystepuje 22?
    cout << "ile 22: " << count(vec.begin(), vec.end(), 22) << endl;

    auto range = equal_range(sorted_vec.begin(), sorted_vec.end(), 22);
    cout << "ile 22: " << (range.second - range.first) << endl;

     cout << "ile 22: " << mset.count(22) << endl;

    // usun wszystkie wieksze od 15
    vec.erase(remove_if(vec.begin(), vec.end(),
                        boost::bind(greater<int>(), _1, 15)), vec.end());

    sorted_vec.erase(upper_bound(sorted_vec.begin(), sorted_vec.end(), 15), sorted_vec.end());

    mset.erase(mset.upper_bound(15), mset.end());
}
