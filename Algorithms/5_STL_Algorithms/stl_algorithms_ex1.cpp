#include "utils.hpp"

#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>
#include <boost/bind.hpp>

using namespace std;

class RandGen
{
    int range_;
public:
    RandGen(int range) : range_(range)
    {
    }

    int operator()() const
    {
        return rand() % range_;
    }
};

bool is_even(int x)
{
    return x % 2 == 0;
}

class AvgCalc
{
    int sum_ = 0;
    int count_ = 0;
public:
    void operator()(int x)
    {
        sum_ += x;
        ++count_;
    }

    double avg() const
    {
        return static_cast<double>(sum_) / count_;
    }
};

int main()
{
    vector<int> vec(25);

    generate(vec.begin(), vec.end(), RandGen(30));

    print(vec, "vec: ");

    // 1a - wyświetl parzyste
    cout << "Parzyste: ";
//    copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "),
//            [](int x) { return x % 2 == 0; });
    remove_copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "),
                   bind(modulus<int>(), placeholders::_1, 2));
    cout << endl;

    // 1b - wyswietl ile jest nieparzystych
    cout << "Ilosc nieparzystych: "
         << count_if(vec.begin(), vec.end(), not1(ptr_fun(&is_even)))
         << endl;

    // 1c - wyswietl ile jest parzystych
    cout << "Ilosc parzystych: "
         << count_if(vec.begin(), vec.end(), [](int x) { return x % 2 == 0; })
         << endl;

    // 2 - usuń liczby podzielne przez 3
    vec.erase(remove_if(vec.begin(), vec.end(),
                        !(boost::bind(modulus<int>(), _1, 3))), vec.end());

    // 3 - tranformacja: podnieś liczby do kwadratu
    //transform(vec.begin(), vec.end(), vec.begin(), [](int x) { return x * x; });
    transform(vec.begin(), vec.end(), vec.begin(), vec.begin(), multiplies<int>());

    // 4 - wypisz 5 najwiekszych liczb
    nth_element(vec.begin(), vec.begin() + 5, vec.end(), greater<int>());
    cout << "5 najwiekszych: ";
    copy(vec.begin(), vec.begin()+5, ostream_iterator<int>(cout, " "));
    cout << endl;

    // 5 - policz wartosc srednia
    AvgCalc calc = for_each(vec.begin(), vec.end(), AvgCalc());

    cout << "avg: " << calc.avg() << endl;
    cout << "avg: " << accumulate(vec.begin(), vec.end(), 0.0) / vec.size() << endl;

    // 6 - wyswietl wszystkie mniejsze od 50
    // TODO
}
