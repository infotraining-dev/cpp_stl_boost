#include <iostream>
#include <set>
#include <map>
#include <unordered_set>
#include <boost/tuple/tuple_comparison.hpp>

using namespace std;

template <typename T1, typename T2>
ostream& operator<<(ostream& out, const pair<T1, T2>& p)
{
    out << "(" << p.first << ", " << p.second << ")";

    return out;
}

template <typename Container>
void print(Container& container, const string& desc)
{
    cout << desc << ": [ ";

    for(typename Container::const_iterator it = container.begin();
        it != container.end(); ++it)
    {
        cout << *it << " ";
    }

    cout << "]" << endl;
}

class Person
{
    int id_;
    string name_;
public:
    Person(int id, const string& name) : id_(id), name_(name)
    {}

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    bool operator<(const Person& p) const
    {
        return boost::tie(id_, name_) < boost::tie(p.id_, p.name_);
    }

    bool operator==(const Person& p) const
    {
        return id_ == p.id_;
    }

    friend class ComparePersonByName;
};

class ComparePersonByName
{
public:
    bool operator()(const Person& p1, const Person& p2) const
    {
        boost::tie(p1.name_, p1.id_) < boost::tie(p2.name_, p2.id_);
    }
};

ostream& operator<<(ostream& out, const Person& p)
{
    out << "Person(id=" << p.id() << ", name=" << p.name() << ")";

    return out;
}

namespace std
{
    template <>
    struct hash<Person>
    {
        size_t operator()(const Person& p) const
        {
            hash<int> int_hasher;
            return int_hasher(p.id());
        }
    };
}
int main()
{
    set<int> set_numbers;

    set_numbers.insert(19);
    set_numbers.insert(9);
    set_numbers.insert(1);
    set_numbers.insert(8);

    print(set_numbers, "set_numbers");

    pair<set<int>::iterator, bool> result = set_numbers.insert(9);

    if (result.second)
    {
        cout << *(result.first) <<
                " został wstawiony" << endl;
    }
    else
        cout << *(result.first) <<
                " był już wstawiony wcześniej" << endl;

    set<int>::iterator where = set_numbers.find(8);

    if (where != set_numbers.end())
    {
        cout << "Znalazlem element " << *where << endl;
    }

    set<Person> people;

    people.insert(Person(1, "Kowalski"));
    people.insert(Person(7, "Nowak"));
    people.insert(Person(3, "Adamska"));

    print(people, "people");

    set<Person, ComparePersonByName> people_by_name(people.begin(), people.end());

    print(people_by_name, "people_by_name");

    map<string, int> map_numbers = { { "One", 1 }, { "Two", 2 },
                                     { "Three", 3}, { "Five", 5 } };

    print(map_numbers, "map_numbers");

    cout << map_numbers["Four"] << endl;

    print(map_numbers, "map_numbers");


    unordered_set<Person> uset_people;

    uset_people.insert(Person(1, "Kowalski"));
    uset_people.insert(Person(7, "Nowak"));
    uset_people.insert(Person(3, "Adamska"));

    cout << "Count person with id = 7: " << uset_people.count(Person(7, "")) << endl;
}








