#include <iostream>
#include <boost/bind.hpp>

using namespace std;

void g(int x, int y, int z)
{
    cout << "g(" << x << ", " << y << ", " << z << ")" << endl;
}

void h(const string& text, int x, double& d)
{
    cout << text << " - x = " << x << endl;
    d = 3.14 * x;
}

class Person
{
    int id_;
    string name_;
public:
    Person(int id, const string& name) : id_(id), name_(name)
    {}

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    void info(const string& prefix)
    {
        cout << prefix << " - id=" << id_ << " - name=" << name_ << endl;
    }

    bool operator<(const Person& p) const
    {
        if (id_ == p.id_)
            return name_ < p.name_;

        return id_ < p.id_;
    }

    bool operator==(const Person& p) const
    {
        return id_ == p.id_;
    }
};


int main()
{
    auto f1 = bind(&g, _1, 10, _2);

    f1(1, 2);

    auto f2 = bind(&g, _1, 9, _1);

    f2(1);

    auto f3 = bind(&g, 1, 2, 3);

    f3();

    int x = 10;
    double d = 0.0;

    auto f4 = bind(&h, "Text", _1, boost::ref(d));

    f4(x);

    cout << "d = " << d << endl;

    Person p(1, "Kowalski");

    auto f5 = bind(&Person::info, &p, "Osoba");

    f5();

}

