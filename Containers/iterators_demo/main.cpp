#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <list>

using namespace std;

template <typename Container>
void print(Container& container, const string& desc)
{
    cout << desc << ": [ ";

    for(typename Container::const_iterator it = container.begin();
        it != container.end(); ++it)
    {
        cout << *it << " ";
    }

    cout << "]" << endl;
}

template <typename InIt, typename OutIt>
void my_copy(InIt start, InIt end, OutIt dest)
{
    while(start != end)
    {
        *(dest++) = *(start++);
    }
}

bool is_odd(int x)
{
    return x % 2;
}

int main()
{
    cout << "Podaj liczby:" << endl;
    istream_iterator<int> start(cin);
    istream_iterator<int> end;

    vector<int> vec(start, end);

    cout << "Items: ";
    copy(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "));
    cout << "\n";

    vector<int> vec_evens;

    remove_copy_if(vec.begin(), vec.end(), back_inserter(vec_evens), &is_odd);

    print(vec_evens, "vec_evens");

    list<int> lst;

    my_copy(vec.begin() + 2, vec.end() -2, back_inserter(lst));

    print(lst, "lst");

    cout << "size: " << distance(vec.begin(), vec.end()) << endl;

    vector<int>::iterator where = vec.begin();

    advance(where, 3);

    cout << "*where = " << *where << endl;
}




