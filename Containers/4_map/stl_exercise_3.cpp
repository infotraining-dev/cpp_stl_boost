#include <map>
#include <iostream>
#include <fstream>
#include <iterator>
#include <set>
#include <string>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;

/*
    Napisz program zliczający ilosc wystapien danego slowa
    w pliku tekstowym. Wyswietl wyniki zaczynajac od najczesciej wystepujacych slow.
*/

typedef vector<string> WordsContainer;

WordsContainer load_words_from_file(const string& file_name)
{
    ifstream file_book(file_name);

    if (!file_book)
    {
        cout << "Blad otwarcia pliku..." << endl;
        exit(1);
    }

    string line;
    WordsContainer words;

    while (getline(file_book, line))
    {
        boost::tokenizer<> tok(line);
        words.insert(words.end(), tok.begin(), tok.end());
    }

    return words;
}

int main()
{
    WordsContainer words = load_words_from_file("../alice.txt");

    map<string, int> concordance;

    // zliczenie ilosci wystapien slow
    for(WordsContainer::const_iterator it = words.begin();
        it != words.end(); ++it)
    {
        concordance[boost::to_lower_copy(*it)]++;
    }

    multimap<int, string> concordance_by_freq;

    for(map<string, int>::iterator it = concordance.begin();
        it != concordance.end(); ++it)
    {
        concordance_by_freq.insert(make_pair(it->second, it->first));
    }

    int counter = 1;
    for(multimap<int, string>::reverse_iterator it = concordance_by_freq.rbegin();
        it != concordance_by_freq.rend(); ++it, ++counter)
    {
        cout << it->second << " - " << it->first << endl;

        if (counter == 10)
            break;
    }
}











