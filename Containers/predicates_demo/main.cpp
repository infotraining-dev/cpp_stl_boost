#include <iostream>
#include <algorithm>
#include <iterator>
#include <functional>
#include <vector>
#include <boost/bind.hpp>
#include <boost/function.hpp>

using namespace std;

class Person
{
    int id_;
    string name_;
public:
    Person(int id, const string& name) : id_(id), name_(name)
    {}

    int id() const
    {
        return id_;
    }

    string name() const
    {
        return name_;
    }

    void info(const string& prefix)
    {
        cout << prefix << " - id=" << id_ << " - name=" << name_ << endl;
    }

    bool operator<(const Person& p) const
    {
        if (id_ == p.id_)
            return name_ < p.name_;

        return id_ < p.id_;
    }

    bool operator==(const Person& p) const
    {
        return id_ == p.id_;
    }
};

class ComparePersonByName
{
public:
    bool operator()(const Person& p1, const Person& p2) const
    {
        if (p1.name() == p2.name())
            return p1.id() < p2.id();

        return p1.name() < p2.name();
    }
};

ostream& operator<<(ostream& out, const Person& p)
{
    out << "Person(id=" << p.id() << ", name=" << p.name() << ")";

    return out;
}

class IsEven : public unary_function<int, bool>
{
public:
    bool operator()(int x) const
    {
        return x % 2 == 0;
    }
};

template <typename InIt, typename Func>
void my_foreach(InIt start, InIt end, Func f) // Func = void (*ptr)(int)
{
    while (start != end)
        f(*(start++));
}

void print_item(int x)
{
    cout << "item: " << x << endl;
}

class Printer
{
public:
    void operator()(int x)
    {
        cout << "Print item: " << x << endl;
    }
};

int main()
{
    vector<int> vec = { 2, 4, 6, 7, 3, 5, 7, 77 };

    vector<int>::iterator where =
            find_if(vec.begin(), vec.end(),
                    bind(greater<int>(), _1, 5));

    if (where != vec.end())
    {
        cout << "Pierwszy element > 5: " << *where << endl;
    }

    vector<int>::iterator garbage_start = remove_if(vec.begin(), vec.end(), bind(greater<int>(), _1, 5));
    vec.erase(garbage_start, vec.end());

    cout << "\nPo remove > 5:\n";

    my_foreach(vec.begin(), vec.end(), Printer());

    vector<int> vec_evens;

    cout << "Evens: ";
    copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "), IsEven());
    cout << endl;

    cout << "Odd: ";

    //copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "), not1(IsEven()));

    copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "),
            [](int x) { return x % 2; });

    cout << endl;

    vector<Person*> people = { new Person(1, "Kowalski"), new Person(6, "Nowak"),
                               new Person(3, "Adamska"), new Person(7, "Nijaki") };

    vector<string> names;

    transform(people.begin(), people.end(), back_inserter(names),
              mem_fn(&Person::name));

    cout << "names: ";
    copy(names.begin(), names.end(), ostream_iterator<string>(cout, " "));
    cout << endl;
    cout << "\nOsoby:\n";

    for_each(people.begin(), people.end(),
             bind(&Person::info, _1, "Osoba")); // ->

    /*
     *  class BoundMethod
     *  {
     *  public:
     *      void operator()(Person* p)
     *      {
     *          p->info("Osoba");
     *      }
     *  };
     *
     */

    boost::function<bool (Person*)> pred_kowalski
            = boost::bind(&Person::name, _1) == "Kowalski";

    vector<Person*>::iterator where_person =
            find_if(people.begin(), people.end(),
                    pred_kowalski);

//    vector<Person*>::iterator where_person =
//            find_if(people.begin(), people.end(),
//                    std::bind(
//                        equal_to<string>(),
//                            std::bind(&Person::name, placeholders::_1),
//                            "Kowalski"));


    if (where_person != people.end())
        (*where_person)->info("Znaleziony");

    for(vector<Person*>::iterator it = people.begin(); it != people.end(); ++it)
        delete *it;
}






