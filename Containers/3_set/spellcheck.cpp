#include <iostream>
#include <fstream>
#include <set>
#include <string>
#include <chrono>
#include <boost/algorithm/string.hpp>
#include <unordered_set>

using namespace std;

int main()
{
    // wszytaj zawartość pliku en.dict ("słownik języka angielskieog")
    // sprawdź poprawość pisowni następującego zdania:
    vector<string> words_list;
    string input_text = "this is an exmple of snetence";
    boost::split(words_list, input_text, boost::is_any_of("\t "));

    ifstream file_in("../en.dict");

    if (!file_in)
        throw runtime_error("File not opened");

    unordered_set<string> dict(128000);

    size_t prev_bucket_count = dict.bucket_count();
    cout << "dict.bucket_count() = " << prev_bucket_count << endl;

    istream_iterator<string> start(file_in);
    istream_iterator<string> end;

    dict.insert(start, end);

    if (prev_bucket_count != dict.bucket_count())
    {
        cout << "Rehashing to " << dict.bucket_count() << " - "
             << dict.load_factor() << endl;
        prev_bucket_count = dict.bucket_count();
    }


    cout << "dict.size() = " << dict.size() << endl;
    cout << "dict.bucket_count() = " << dict.bucket_count() << endl;
    cout << "dict.load_factor() = " << dict.load_factor() << endl;

    for(vector<string>::const_iterator it = words_list.begin();
        it != words_list.end(); ++it)
    {
        if (!dict.count(*it))
        {
            cout << "misspelled: " << *it << endl;
        }
    }
}

