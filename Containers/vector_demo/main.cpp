#include <iostream>
#include <vector>
#include <string>

using namespace std;

template <typename Container>
void print(const Container& cont, const string& desc)
{
    cout << desc << ": [ ";

    for(auto& item : cont)
        cout << item << " ";

    cout << "]\n";
}

template <typename T>
void print_stat(const vector<T>& vec, const string& desc)
{
    cout << desc << ": size=" << vec.size()
         << " capacity=" << vec.capacity() << endl;
}

int main()
{
    vector<int> vec1;
    print_stat(vec1, "vec1");

    vector<int> vec2(10);
    print_stat(vec2, "vec2");
    print(vec2, "vec2");

    vector<int> vec3 = { 1, 6, 7, 8, 10 };
    print_stat(vec3, "vec3");
    print(vec3, "vec3");

    vector<int> vec4 = move(vec3);

    cout << "\nAfter move:" << endl;
    print_stat(vec3, "vec3");
    print(vec3, "vec3");

    print_stat(vec4, "vec4");
    print(vec4, "vec4");

    vec4.push_back(11);
    print_stat(vec4, "vec4");

    vector<int> vec5;

    vec5.reserve(1000);
    size_t prev_capacity = vec5.capacity();
    for(int i = 0; i < 1000; ++i)
    {
        vec5.push_back(i);

        if (vec5.capacity() != prev_capacity)
        {
            print_stat(vec5, "vec5");
            prev_capacity  = vec5.capacity();
        }
    }

    vec5.clear();

    vector<int>(vec5).swap(vec5);

    //vec5.shrink_to_fit();  // c++11
    print_stat(vec5, "vec5 po clear");

    cout << "max size vec5: " << vec5.max_size() << endl;

    int numbers[] = { 4, 5, 6, 7, 8, 10 };

    vector<int> vec6(numbers, numbers + 6);

    print(vec6, "vec6");

    for(vector<int>::const_iterator it = vec6.begin();
        it  != vec6.end(); ++it)
    {
        cout << *it << " ";
    }
    cout << endl;

    for(vector<int>::reverse_iterator it = vec6.rbegin();
        it  != vec6.rend(); ++it)
    {
        cout << *it << " ";
    }
    cout << endl;
}

