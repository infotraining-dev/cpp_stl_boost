#include <stdio.h>
#include <stdexcept>
#include <iostream>
#include <thread>
#include <mutex>
#include <memory>
#include <boost/shared_ptr.hpp>

const char* get_line()
{
	static size_t count = 0;

	if (++count == 13)
		throw std::runtime_error("Blad!!!");

	return "Hello RAII\n";
}

void save_to_file(const char* file_name)
{
	FILE* file = fopen(file_name, "w");

	if ( file == 0 )
		throw std::runtime_error("Blad otwarcia pliku!!!");

	for(size_t i = 0; i < 100; ++i)
		fprintf(file, get_line());

	fclose(file);
}


class FileGuard
{
    FILE* file_;
public:
    FileGuard(FILE* file) : file_(file)
    {
    }

    FileGuard(const FileGuard&) = delete;
    FileGuard& operator=(const FileGuard&) = delete;

    // konstruktor przenoszacy
    FileGuard(FileGuard&& source) : file_(source.file_)
    {
        std::cout << "Move constructor" << std::endl;
        source.file_ = 0;
    }

    FileGuard& operator=(FileGuard&& source)
    {
        this->~FileGuard();
        file_ = source.file_;
        source.file_ = NULL;

        return *this;
    }

    ~FileGuard()
    {
        if (file_)
            fclose(file_);
    }

    FILE* get() const
    {
        return file_;
    }
};

std::mutex file_mtx;

void save_to_file_with_raii(const char* file_name)
{
    std::lock_guard<std::mutex> lk(file_mtx);

    FileGuard file(fopen(file_name, "w"));

    FileGuard file2 = std::move(file);

    if (file2.get() == 0)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file2.get(), get_line());
}

void save_to_file_with_sp(const char* file_name)
{
    FILE* raw_file = fopen(file_name, "w");

    if (!raw_file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    boost::shared_ptr<FILE> file(raw_file, &fclose);

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

void save_to_file_with_up(const char* file_name)
{
    std::unique_ptr<FILE, int(*)(FILE*)> file(fopen(file_name, "w"), &fclose);

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

int main()
try
{
    //save_to_file("text.txt");
    //save_to_file_with_raii("text.txt");
    //save_to_file_with_sp("text.txt");
    save_to_file_with_up("text.txt");
}
catch(const std::exception& e)
{
	std::cout << e.what() << std::endl;

    std::cout << "Press a key...";

    std::string temp;
    std::getline(std::cin, temp);
}
