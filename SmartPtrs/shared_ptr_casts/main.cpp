#include <iostream>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

using namespace std;

class Base
{
public:
    Base()
    {
        cout << "Base()" << endl;
    }

    ~Base()
    {
        cout << "~Base()" << endl;
    }

    virtual void do_sth()
    {
        cout << "Base::do_sth()" << endl;
    }
};

class Derived : public Base
{
public:
    Derived()
    {
        cout << "Derived()" << endl;
    }

    ~Derived()
    {
        cout << "~Derived()" << endl;
    }

    virtual void do_sth()
    {
        cout << "Derived::do_sth()" << endl;
    }

    void extra()
    {
        cout << "Derived::extra()" << endl;
    }
};

int main()
{
    {
        Base* ptr_base = new Derived();

        ptr_base->do_sth();

        Derived* ptr_dev = static_cast<Derived*>(ptr_base);

        ptr_dev->extra();

        delete ptr_base;
    }

    cout << "\n----------------\n";

    {
        boost::shared_ptr<Base> base_ptr = boost::make_shared<Derived>();

        base_ptr->do_sth();

        boost::shared_ptr<Derived> derived_ptr =
                boost::static_pointer_cast<Derived>(base_ptr);

        derived_ptr->extra();
    }
}

