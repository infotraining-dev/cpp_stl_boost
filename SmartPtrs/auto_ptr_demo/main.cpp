#include <iostream>
#include <memory>
#include <stdexcept>
#include <cassert>
#include <vector>
#include <boost/scoped_ptr.hpp>

using namespace std;

class Object
{
    int value_;
public:
    Object(int value = 0) : value_(value)
    {
        cout << "Object(" << value_ << ")" << endl;
    }

    ~Object()
    {
        cout << "~Object(" << value_ << ")" << endl;
    }

    int value() const
    {
        return value_;
    }
};

void may_throw(int arg)
{
    if (arg % 13 == 0)
        throw runtime_error("Error#13");
}

auto_ptr<Object> create(int v)
{
    return auto_ptr<Object>(new Object(v));
}

void sink(auto_ptr<Object>& arg)
{
    cout << "Sink: " << arg->value() << endl;
}

void local_fun()
{
    create(2);

    auto_ptr<Object> ap1 = create(1);

    auto_ptr<Object> ap2 = ap1;

    assert(ap1.get() == NULL);

    sink(ap2);

    //sink(create(7));

    may_throw(26);
}

unique_ptr<Object> create_up(int v)
{
    return unique_ptr<Object>(new Object(v));
}

void sink_up(unique_ptr<Object> ptr)
{
    cout << "Sink up: " << ptr->value() << endl;
}

void local_fun_with_up()
{
    unique_ptr<Object> up1 = create_up(7);

    cout << "up1: " << up1->value() << endl;

    unique_ptr<Object> up2 = move(up1);

    sink_up(create_up(9));

    sink_up(move(up2));

    vector<unique_ptr<Object>> vec;

    up2 = create_up(10);

    vec.push_back(create_up(12));
    vec.push_back(move(up2));
    vec.emplace_back(new Object(15));

    unique_ptr<Object[]> array_obj(new Object[10]);

    //array_obj[0].value();

    may_throw(13);
}

void local_fun_with_scoped_ptr()
{
    boost::scoped_ptr<Object> ptr1(new Object(89));

    cout << "(*ptr1).value() =  " << ptr1->value() << endl;
}

int main()
try
{
    //local_fun();
    local_fun_with_up();
}
catch(const exception& e)
{
    cout << e.what() << endl;
}

